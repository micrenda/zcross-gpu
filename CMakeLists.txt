cmake_minimum_required(VERSION 3.13)
project(zcross-gpu)

include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

set(ENABLE_CUDA 	OFF CACHE BOOL "Enable CUDA accelleration" )
set(ENABLE_SYCL 	OFF CACHE BOOL "Enable SYCL accelleration" )
set(ENABLE_OPENCL 	OFF CACHE BOOL "Enable OpenCl accelleration" )


if (${ENABLE_CUDA})
	enable_language(CUDA)
	add_compile_definitions(COMPUTE_CUDA)
endif()

if (${ENABLE_SYCL})
	message(FATAL_ERROR "SYCL backend not yet implemented")
endif()

if (${ENABLE_OPENCL})
	message(FATAL_ERROR "OpenCl backend not yet implemented")
endif()

if (NOT ${ENABLE_CUDA} AND NOT ${ENABLE_SYCL} AND NOT ${ENABLE_OPENCL})
	message(FATAL_ERROR "At least one of ENABLE_CUDA, ENABLE_SYCL or ENABLE_OPENCL must be selected.")
endif()

include_directories(${PROJECT_SOURCE_DIR}/include)
include_directories(${PROJECT_SOURCE_DIR}/external/qtydef/include)

file(GLOB SOURCE_FILES 		src/ZCrossGpu.cpp)

if (${ENABLE_CUDA})
	list(APPEND SOURCE_FILES src/ZCrossCudaImpl.cu)
endif()

add_library(zcross_gpu SHARED ${SOURCE_FILES})
add_library(DFPE::zcross_gpu ALIAS zcross_gpu)

find_package (ZCross REQUIRED)
target_link_libraries (zcross_gpu PUBLIC DFPE::zcross)
	
file(GLOB HEADERS ${PROJECT_SOURCE_DIR}/include/*.hpp )
set_target_properties(zcross_gpu PROPERTIES PUBLIC_HEADER "${HEADERS}")

target_include_directories(zcross_gpu PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
    
# Installing
install(TARGETS zcross_gpu
	EXPORT                          ZCrossGpuConfig
	ARCHIVE                         DESTINATION ${CMAKE_INSTALL_LIBDIR}
	RUNTIME                         DESTINATION ${CMAKE_INSTALL_BINDIR}
	LIBRARY                         DESTINATION ${CMAKE_INSTALL_LIBDIR}
	PUBLIC_HEADER           		DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
	
install(EXPORT ZCrossGpuConfig NAMESPACE DFPE:: DESTINATION  share/zcross_gpu/cmake)
export(TARGETS zcross_gpu FILE ZCrossGpuConfig.cmake)

if(NOT EXISTS "${PROJECT_SOURCE_DIR}/external/qtydef/include")
   message(FATAL_ERROR "Unable to find the directory 'external/qtydef/include'.
Did you perform?
  git submodule init
  git submodule update")
endif()
