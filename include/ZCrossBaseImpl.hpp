#pragma once

namespace dfpe
{
	class ZCrossBaseImpl
	{
		
	public:
		virtual void initialize() = 0;
		virtual int  interpolate(bool use_density, double energy, double& results_sum, std::vector<double>* results = nullptr) const = 0;
	
	
	protected:	
	int in_size  = 0;
    int out_size = 0;
    
	std::vector<double> energy_data;
    std::vector<double> area_data;
    std::vector<double> density_data;

    std::vector<double> fit_lower_a_data;
    std::vector<double> fit_lower_k_data;
    std::vector<double> fit_upper_a_data;
    std::vector<double> fit_upper_k_data;

    std::vector<int>    node_type_data;
    std::vector<int>    out_index_data;
    
    friend class ZCrossGpu; 
	};
}
