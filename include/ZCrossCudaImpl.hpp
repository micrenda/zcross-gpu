#pragma once

#include <map>
#include <vector>
#include "ZCrossBaseImpl.hpp"

namespace dfpe
{
	class ZCrossCudaImpl: public ZCrossBaseImpl
	{
	private:
		// gpu buffers
		
		double *energy_buffer;
		double *area_buffer;
		double *density_buffer;

        double *fit_lower_a_buffer;
		double *fit_lower_k_buffer;
        double *fit_upper_a_buffer;
		double *fit_upper_k_buffer;


		int    *node_type_buffer;
		int    *out_index_buffer;
		// gpu output buffer
		double *out_data_buffer;
		double *out_sum_buffer;
		// pinned host memory
		double *out_data;
		// block and
		int grid_size;
		int block_size;
		
		
	public:
		ZCrossCudaImpl();
		virtual ~ZCrossCudaImpl();

		virtual void initialize() override;
		virtual int  interpolate(bool use_density, double energy, double& results_sum, std::vector<double>* results = nullptr) const override;
	};
}
