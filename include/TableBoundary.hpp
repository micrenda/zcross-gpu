#pragma once
namespace dfpe
{
    enum class TableBoundary
    {
        LOWER_WITH_EXCEPTION = -4,
        LOWER_WITH_FIT       = -3,
        LOWER_WITH_BOUNDARY  = -2,
        LOWER_WITH_ZERO      = -1,

        MIDDLE = 0,

        UPPER_WITH_ZERO      = 1,
        UPPER_WITH_BOUNDARY  = 2,
        UPPER_WITH_FIT       = 3,
        UPPER_WITH_EXCEPTION = 4,
    };
}