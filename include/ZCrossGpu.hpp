#pragma once

#include <vector>
#include <map>
#include <memory>
#include "ZCrossCudaImpl.hpp"
#include "Specie.hpp"
#include "QtyDefinitions.hpp"
#include "IntScatteringTable.hpp"
#include "OutOfTableMode.hpp"
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
namespace dfpe
{
	
	/// derived dimension for number density in SI units : L⁻³
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<-3>>
    >>::type     																	number_density_dimension;
    typedef boost::units::unit<number_density_dimension, boost::units::si::system>	number_density;
    typedef boost::units::quantity<number_density>                    				QtySiNumberDensity;
	

	
	class ZCrossGpu
	{
		
	public:
		ZCrossGpu();
		virtual ~ZCrossGpu() {};
		
		void load (
			const Specie& target,
			const IntScatteringTable& table, 
			QtySiNumberDensity density,
			const OutOfTableMode& lowerBoundaryMode, 
			const OutOfTableMode& upperBoundaryMode);
	
		void load(
			const Specie& target,
			const std::vector<IntScatteringTable>& tables,
			QtySiNumberDensity density = QtySiNumberDensity(),
			const OutOfTableMode& lowerBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION, 
			const OutOfTableMode& upperBoundaryMode = OutOfTableMode::LAUNCH_EXCEPTION);
		
		QtySiWavenumber getSumInteractionCoefficients(const QtySiEnergy& energy) const;

		QtySiArea getSumCrossSections(const QtySiEnergy& energy) const;
	
		void getInteractionCoefficients(const QtySiEnergy& energy, std::vector<QtySiWavenumber>& results) const;
			
		void getCrossSections(const QtySiEnergy& energy, std::vector<QtySiArea>& results) const;

		void initialize() { base->initialize(); };

	public:
		std::shared_ptr<ZCrossBaseImpl> base;
	
	protected:
	void checkOverflow(int overflow, const QtySiEnergy& energy) const;
    protected:
    
	QtySiArea   		unit_area = QtySiArea(1. * 
		boost::units::si::pico * boost::units::si::meter * 
		boost::units::si::pico * boost::units::si::meter); // pm^2
	QtySiEnergy 		unit_energy = QtySiEnergy(1. * boost::units::si::volt  * boost::units::si::constants::codata::e);	  // 1 eV
	QtySiNumberDensity 	unit_density = QtySiNumberDensity(1. / 
		boost::units::si::pico / boost::units::si::meter / 
		boost::units::si::pico / boost::units::si::meter / 
		boost::units::si::pico / boost::units::si::meter);  // 1. pm^-3
	QtySiWavenumber 	unit_wavenumber = QtySiWavenumber(1. / boost::units::si::pico / boost::units::si::meter );  // 1. pm^-1
	
	};
}
