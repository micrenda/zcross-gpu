
#include <iostream>
#include <stdio.h>
#include "TableBoundary.hpp"
#include "ZCrossCudaImpl.hpp"
#include <stdexcept>
#include <cuda.h>


using namespace dfpe;

#if defined __CUDA_ARCH__ && __CUDA_ARCH__ < 600
__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull = (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                               __longlong_as_double(assumed)));

    // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
    } while (assumed != old);
    return __longlong_as_double(old);
}
#endif

// cuda kernel
__global__
void compute(
	int	   in_size,
	int	   out_size,
	bool   use_density, 
	double energy,
	double *energy_buffer,
	double *area_buffer,
	double *density_buffer,
    double *fit_lower_a_buffer,
    double *fit_lower_k_buffer,
    double *fit_upper_a_buffer,
    double *fit_upper_k_buffer,
	int    *node_type_buffer,
	int    *out_index_buffer,
	double *out_data_buffer,
	double *out_sum_buffer,
	int    *overflow)
{

    auto i_i = threadIdx.x + blockIdx.x * blockDim.x ;
    
     
    //   We do this check because there is the possibility that blockDim * gridDim > number of threades needed (in_size)
    //   in that case it will couse overflow and bad things happen.
    if (i_i < in_size)
    {
		auto n_t = node_type_buffer[i_i];
		auto o_i = out_index_buffer[i_i];

		bool is_middle = false;
		bool is_under  = false;
		bool is_over   = false;

		if (n_t < (int)TableBoundary::MIDDLE)
		{
			is_middle = (energy >= energy_buffer[i_i]) && (energy < energy_buffer[i_i + 1]);
			is_under  =  energy <  energy_buffer[i_i];
		}
		else if (n_t > (int)TableBoundary::MIDDLE)
		{
			is_middle = false;
			is_over   = energy >= energy_buffer[i_i];
		}
		else
		{
			is_middle = (energy >= energy_buffer[i_i]) && (energy < energy_buffer[i_i + 1]);
		}


		if (is_middle)
		{
			out_data_buffer[o_i] = area_buffer[i_i] + ((area_buffer[i_i + 1] - area_buffer[i_i]) * (energy - energy_buffer[i_i]) / (energy_buffer[i_i + 1] - energy_buffer[i_i]));
		}
		else if (is_under)
		{
			switch(n_t)
			{
				case (int) TableBoundary::LOWER_WITH_ZERO:
					out_data_buffer[o_i] = 0;
				break;

				case (int) TableBoundary::LOWER_WITH_BOUNDARY:
					out_data_buffer[o_i] = area_buffer[i_i];
				break;

                case (int) TableBoundary::LOWER_WITH_FIT:
                    out_data_buffer[o_i] = fit_lower_a_buffer[o_i] * pow(energy, fit_lower_k_buffer[o_i]);
                break;

				case (int) TableBoundary::LOWER_WITH_EXCEPTION:
					*overflow = -1;
				break;
			}
		}
		else if (is_over)
		{
			switch(n_t)
			{
				case (int) TableBoundary::UPPER_WITH_ZERO:
					out_data_buffer[o_i] = 0;
				break;

				case (int) TableBoundary::UPPER_WITH_BOUNDARY:
					out_data_buffer[o_i] = area_buffer[i_i];
				break;

				case (int) TableBoundary::UPPER_WITH_FIT:
                    out_data_buffer[o_i] = fit_upper_a_buffer[o_i] * pow(energy, fit_upper_k_buffer[o_i]);
                break;

                case (int) TableBoundary::UPPER_WITH_EXCEPTION:
					*overflow =  1;
				break;
			}
		}

		if (is_middle || is_under || is_over)
		{
			if (use_density)
				out_data_buffer[o_i] *= density_buffer[i_i];

			atomicAdd(out_sum_buffer, out_data_buffer[o_i]);
		}
		
			
		//if (is_middle || is_under || is_over)
		//	printf("%d, energy %e (%e), n_t %d, middle %d under %d over %d = %e (%d)\n",  i_i, energy, energy_buffer[i_i], n_t, is_middle, is_under, is_over, out_data_buffer[o_i], o_i);
	}
}

ZCrossCudaImpl::ZCrossCudaImpl() :
	energy_buffer(nullptr),
	area_buffer(nullptr),
	density_buffer(nullptr),
    fit_lower_a_buffer(nullptr),
    fit_lower_k_buffer(nullptr),
    fit_upper_a_buffer(nullptr),
    fit_upper_k_buffer(nullptr),
	node_type_buffer(nullptr),
	out_index_buffer(nullptr),
	out_data_buffer(nullptr),
	out_sum_buffer(nullptr),
	out_data(nullptr)
{}

ZCrossCudaImpl::~ZCrossCudaImpl()
{
    cudaFreeHost(out_data);
    cudaFree(out_data_buffer);
    cudaFree(out_sum_buffer);
    cudaFree(fit_lower_a_buffer);
    cudaFree(fit_lower_k_buffer);
    cudaFree(fit_upper_a_buffer);
    cudaFree(fit_upper_k_buffer);
    cudaFree(energy_buffer);
    cudaFree(area_buffer);
    cudaFree(density_buffer);
    cudaFree(node_type_buffer);
    cudaFree(out_index_buffer);
}

void ZCrossCudaImpl::initialize()
{
    int aux;
    cudaOccupancyMaxPotentialBlockSize(&aux, &block_size, compute, 0, in_size);
    grid_size = (in_size + block_size - 1) / block_size;

    cudaMalloc((void**)&energy_buffer,    in_size * sizeof(double));
    cudaMalloc((void**)&area_buffer,      in_size * sizeof(double));
    cudaMalloc((void**)&density_buffer,   in_size * sizeof(double));
    cudaMalloc((void**)&out_index_buffer, in_size * sizeof(int));
    cudaMalloc((void**)&node_type_buffer, in_size * sizeof(int));

    cudaMalloc((void**)&fit_lower_a_buffer, out_size * sizeof(double));
    cudaMalloc((void**)&fit_lower_k_buffer, out_size * sizeof(double));
    cudaMalloc((void**)&fit_upper_a_buffer, out_size * sizeof(double));
    cudaMalloc((void**)&fit_upper_k_buffer, out_size * sizeof(double));

    cudaMemcpy(energy_buffer,    energy_data.data(),    in_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(area_buffer,      area_data.data(),      in_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(density_buffer,   density_data.data(),   in_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(node_type_buffer, node_type_data.data(), in_size * sizeof(int),    cudaMemcpyHostToDevice);
    cudaMemcpy(out_index_buffer, out_index_data.data(), in_size * sizeof(int),    cudaMemcpyHostToDevice);

    cudaMemcpy(fit_lower_a_buffer, fit_lower_a_data.data(), out_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(fit_lower_k_buffer, fit_lower_k_data.data(), out_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(fit_upper_a_buffer, fit_upper_a_data.data(), out_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(fit_upper_k_buffer, fit_upper_k_data.data(), out_size * sizeof(double), cudaMemcpyHostToDevice);

    cudaMallocHost((void**)&out_data_buffer, out_size * sizeof(double));
    cudaMallocHost((void**)&out_sum_buffer, sizeof(double));
    //cudaMallocHost((void**)&out_data,    out_size * sizeof(double));
    
}

int ZCrossCudaImpl::interpolate(bool use_density, double energy, double& results_sum, std::vector<double>* results) const {

    // memset(out_data, .0, out_size * sizeof(double));
    // cudaMemcpy(out_data_buffer, out_data, out_size * sizeof(double), cudaMemcpyHostToDevice);
    
   int overflow = 0;
	
	//cudaMemset(out_data_buffer, 0., out_size * sizeof(double));
    results_sum = 0.;
    cudaMemcpy(out_sum_buffer,    &results_sum,   sizeof(double), cudaMemcpyHostToDevice);
    
    compute<<<grid_size, block_size>>>(
									in_size,
									out_size,
									use_density,
									energy,
                                    energy_buffer,
                                    area_buffer,
                                    density_buffer,
                                    fit_lower_a_buffer,
                                    fit_lower_k_buffer,
                                    fit_upper_a_buffer,
                                    fit_upper_k_buffer,
                                    node_type_buffer,
                                    out_index_buffer,
                                    out_data_buffer,
                                    out_sum_buffer,
                                    &overflow);

	                                  
    auto status = cudaDeviceSynchronize();

    if (status != cudaSuccess)
    {
        throw std::runtime_error(cudaGetErrorString(status));
        //throw std::runtime_error(cudaGetErrorName(status));
    }

    if (results != nullptr)
    {
		//cudaMemcpy(results->data(), out_data_buffer, out_size * sizeof(double), cudaMemcpyDeviceToHost);
		//cudaMemcpy(out_data, out_data_buffer, out_size * sizeof(double), cudaMemcpyDeviceToHost);
		memcpy(results->data(), out_data_buffer, out_size * sizeof(double));
    }
		
	results_sum = out_sum_buffer[0];
	//cudaMemcpy(&results_sum, out_sum_buffer,  sizeof(double), cudaMemcpyDeviceToHost);
	
    return overflow;
}
