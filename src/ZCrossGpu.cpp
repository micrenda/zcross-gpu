#include <vector>
#include <iostream>
#include <cstring>
#include <memory>
#include "ZCrossGpu.hpp"
#include "ZCrossCudaImpl.hpp"
#include "TableBoundary.hpp"
#include "ZCrossError.hpp"

using namespace std;
using namespace dfpe;


ZCrossGpu::ZCrossGpu()
{
	base = make_shared<ZCrossCudaImpl>();
}
	
void ZCrossGpu::load (
	const Specie& target,
	const vector<IntScatteringTable>& tables, 
	QtySiNumberDensity density,
	const OutOfTableMode& lowerBoundaryMode, 
	const OutOfTableMode& upperBoundaryMode)
{
	for(const IntScatteringTable &table : tables)
		load(target, table, density, lowerBoundaryMode, upperBoundaryMode);
}

void ZCrossGpu::load (
	const Specie& target,
	const IntScatteringTable& table, 
	QtySiNumberDensity density,
	const OutOfTableMode& lowerBoundaryMode, 
	const OutOfTableMode& upperBoundaryMode)
{

	auto& list = table.getTable();
	for (const pair<QtySiEnergy, QtySiArea> &pair : list)
	{
		base->node_type_data.push_back((int)TableBoundary::MIDDLE);
		base->energy_data.push_back(pair.first / unit_energy);
		base->area_data.push_back(pair.second  / unit_area);
		base->out_index_data.push_back(base->out_size);
		base->density_data.push_back(density   / unit_density);
	}

	base->fit_lower_a_data.push_back(table.getFitLowerA() / unit_area);
	base->fit_lower_k_data.push_back(table.getFitLowerK());
	base->fit_upper_a_data.push_back(table.getFitUpperA() / unit_area);
	base->fit_upper_k_data.push_back(table.getFitUpperK());


	size_t start = base->in_size;
	base->in_size  += table.getTable().size();
	base->out_size += 1;	
	size_t end    = base->in_size - 1;
	
	if (!list.empty())
	{
		switch (lowerBoundaryMode)
		{
			case OutOfTableMode::ZERO_VALUE:
				base->node_type_data[start] = (int) TableBoundary::LOWER_WITH_ZERO;
			break;
			
			case OutOfTableMode::BOUNDARY_VALUE:
				base->node_type_data[start] = (int) TableBoundary::LOWER_WITH_BOUNDARY;
			break;

		    case OutOfTableMode::FIT_VALUE:
				base->node_type_data[start] = (int) TableBoundary::LOWER_WITH_FIT;
			break;
			
			case OutOfTableMode::LAUNCH_EXCEPTION:
				base->node_type_data[start] = (int) TableBoundary::LOWER_WITH_EXCEPTION;
			break;
			
			default:
				throw std::logic_error("Unknown value for lowerBoundaryMode");
			
		}	
	
		switch (upperBoundaryMode)
		{
			case OutOfTableMode::ZERO_VALUE:
				base->node_type_data[end] = (int) TableBoundary::UPPER_WITH_ZERO;
			break;
			
			case OutOfTableMode::BOUNDARY_VALUE:
				base->node_type_data[end] = (int) TableBoundary::UPPER_WITH_BOUNDARY;
			break;

			case OutOfTableMode::FIT_VALUE:
				base->node_type_data[end] = (int) TableBoundary::UPPER_WITH_FIT;
			break;
			
			case OutOfTableMode::LAUNCH_EXCEPTION:
				base->node_type_data[end] = (int) TableBoundary::UPPER_WITH_EXCEPTION;
			break;
			
			default:
				throw std::logic_error("Unknown value for upperBoundaryMode");
			
		}	
	}
	
	

}


QtySiWavenumber ZCrossGpu::getSumInteractionCoefficients(
	const QtySiEnergy& energy) const
{
	
	double sum = 0;
	int overflow = base->interpolate(true, energy / unit_energy, sum);
	checkOverflow(overflow, energy);	
	return sum * unit_wavenumber;
}

QtySiArea ZCrossGpu::getSumCrossSections(
	const QtySiEnergy& energy) const
{
	double sum = 0;
	int overflow = base->interpolate(false, energy / unit_energy, sum);
	checkOverflow(overflow, energy);
	
	return sum * unit_area;
}
	
void ZCrossGpu::getInteractionCoefficients(
	const QtySiEnergy& energy, 
	vector<QtySiWavenumber>& results) const
{
	double sum = 0;
	vector<double> buffer(base->out_size);
	int overflow = base->interpolate(true, energy / unit_energy, sum, &buffer);
	checkOverflow(overflow, energy);
	
	size_t i = 0;
	for (double& value: buffer)
		results[i++] = value * unit_wavenumber;
}

void ZCrossGpu::getCrossSections(
	const QtySiEnergy& energy, 
	vector<QtySiArea>& results) const
{
	double sum = 0;
	vector<double> buffer(results.size());
	int overflow = base->interpolate(false, energy / unit_energy, sum, &buffer);
	checkOverflow(overflow, energy);
	
	size_t i = 0;
	for (double& value: buffer)
		results[i++] = value * unit_area;
}

void ZCrossGpu::checkOverflow(int overflow, const QtySiEnergy& energy) const
{
	if (overflow < 0)
		throw ZCrossError("Impossible to get a valid cross section for energy " + to_string((energy / ZCrossTypes::electronvoltEnergy).value()) + " eV: the value is too low");	
	if (overflow > 0)
		throw ZCrossError("Impossible to get a valid cross section for energy " + to_string((energy / ZCrossTypes::electronvoltEnergy).value()) + " eV: the value is too high");	
	
}
