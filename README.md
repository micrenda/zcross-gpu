# ZCross GPU

Library to add GPU acceleration to [Betabolt](https://gilab.com/micrenda/betaboltz) projects.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

[Boost](https://www.boost.org/)
[ZCross](https://github.com/micrenda/zcross)
[Betaboltz](https://github.com/micrenda/betaboltz)

### Installing

(work in progress)

## Built With

* C++17
* [CMake++](https://cmake.org/)

## Authors

* **Calin Banu**    - Initial work
* **Michele Renda** - Integration - [email](mailto:michele.renda@cern.ch)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the LGPL License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* We would like to thanks Calin Bira and Calin Alexa for their significative support.

